/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - This application demonstrates the use I2C CMSIS-Driver to operate the
 *    I2C interface. It configures the I2C as slave and calls "SlaveReceive".
 *    A new transfer starts by the master (SPI0) when the button (DIO5)
 *    is pressed.
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* Global variables */
/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system ans drivers, then send an I2C frame 
 controlled on button press.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void) {
	/* Initialize the clocks and configure button press interrupt */
	Initialize();
	LCD_BACKLIGHT(ON);
	PRINTF("DEVICE INITIALIZED\n");
	char *text = "Soil Moist: 89%";
	char *text1 = "Temp: 27*C";

	while (1) {
		//LCD_BACKLIGHT(OFF);
		LCD_SETCURSOR(1, 1);
		LCD_PRINT(text);

		//__delay_ms(3000);
		//LCD_BACKLIGHT(ON);
		LCD_SETCURSOR(2, 1);
		LCD_PRINT(text1);
		//I2cTransmit(0, 0xAA);
		__delay_ms(3000);
		LCD_CLEAR_SCREEN();
		__delay_ms(1000);
	}

}
