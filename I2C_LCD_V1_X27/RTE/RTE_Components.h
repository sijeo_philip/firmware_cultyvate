/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: i2c_cmsis_driver
 * RTE configuration: i2c_cmsis_driver.rteconfig
*/
#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

/*
 * Define the Device Header File:
*/
#define CMSIS_device_header "rsl10.h"

/* ONSemiconductor::CMSIS Driver.I2C.I2C.source */
#define RTECFG_I2C
/* ONSemiconductor::Device.Libraries.DMA.source */
#define RTECFG_DMA
/* ONSemiconductor::Device.Libraries.GPIO.source */
#define RTECFG_GPIO

#endif /* RTE_COMPONENTS_H */
