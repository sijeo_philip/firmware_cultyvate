#include "app.h"
#include "LCD.h"
#include <printf.h>

#define LCD_WAIT_DELAY 2
//#define LCD_BL 0x00
#define LCD_EN 0x04
#define LCD_RW 0x02
#define LCD_RS 0x01

#define LCD_INIT_BYTE           0x30  // 0011 0000
#define LCD_BUS_WIDTH_4BIT      0x20  // 0010 0000
#define LCD_BUS_WIDTH_8BIT      0x30  // 0011 0000
#define LCD_CLEAR               0x01  // 0000 0001
#define LCD_HOME                0x02  // 0000 0010
#define LCD_ON                  0x0C  // 0000 1100
#define LCD_OFF                 0x08  // 0000 1000
#define LCD_LINE1               0x80  // 1000 0000
#define LCD_LINE2               0xC0  // 1100 0000
#define LCD_LINE3               0x94  // 1001 0100
#define LCD_LINE4               0xD4  // 1101 0100
#define LCD_CURSOR_OFF          0x0C  // 0000 1100
#define LCD_UNDERLINE_ON        0x0E  // 0000 1110
#define LCD_BLINK_CURSOR_ON     0x0F  // 0000 1111
#define LCD_MOVE_CURSOR_LEFT    0x10  // 0001 0000
#define LCD_MOVE_CURSOR_RIGHT   0x14  // 0001 0100
#define LCD_SHIFT_LEFT          0x18  // 0001 1000
#define LCD_SHIFT_RIGHT         0x1E  // 0001 1110

#define LCD_DISPLAY_ON_CURSOR_OFF               0x0C
#define LCD_DISPLAY_OFF_CURSOR_OFF_BLINK_OFF    0x08
#define LCD_4BITS_2LINES_5x8FONT                0x28
#define LCD_INCREMENT_NO_SHIFT                  0x06

#define LO_NIBBLE(b) (((b) << 4) & 0xF0)
#define HI_NIBBLE(b) ((b) & 0xF0)

#define LCD_BACKLIGHT_ON                        0x01
#define LCD_BACKLIGHT_OFF                       0x00

void LcdInit();
void LcdWriteCommand(unsigned char data, unsigned char cmdtype);
void LcdWriteChar(unsigned char data);
//void LCD_PRINT(char *s);
//int LCD_SETCURSOR(unsigned char row, unsigned char column);
//void clear(void);

DRIVER_GPIO_t *gpio;
ARM_DRIVER_I2C *i2c;

/* Data buffers */
uint8_t buff_tx[] __attribute__ ((aligned(4))) = "RSL10 I2C TEST";
uint8_t buff_rx[sizeof(buff_tx)] __attribute__ ((aligned(4)));
size_t buff_size = sizeof(buff_tx);
//unsigned char BACKLIGHT_STATE;

static unsigned char BACKLIGHT_STATE = 0x01;
static unsigned char LCD_BL;

int LCD_BACKLIGHT(unsigned char value)
{
	if (value == LCD_BACKLIGHT_ON)
	{
		BACKLIGHT_STATE = LCD_BACKLIGHT_ON;
		LCD_BL = 0x08;
	}
	else if (value == LCD_BACKLIGHT_OFF)
	{
		BACKLIGHT_STATE = LCD_BACKLIGHT_OFF;
		LCD_BL = 0x00;
	}
	return 0;
}
void I2cTransmit(uint8_t add, unsigned char Data)
{
	/* Start transmission as master */
	i2c->MasterTransmit(RTE_I2C0_SLAVE_ADDR_DEFAULT, &Data, sizeof(Data), false);
	__delay_ms(0.2);
	//while(i2c->GetStatus().busy!=1U);
	i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);

}

void __delay_ms(float delay)
{
	delay/=1000;
	Sys_Delay_ProgramROM(delay * SystemCoreClock);
}


void LcdInit()
{
    LcdWriteCommand(LCD_INIT_BYTE, 0);
    __delay_ms(30);
    LcdWriteCommand(LCD_INIT_BYTE, 0);
    LcdWriteCommand(LCD_INIT_BYTE, 0);

    LcdWriteCommand(LCD_BUS_WIDTH_4BIT, 0);
    LcdWriteCommand(LCD_4BITS_2LINES_5x8FONT, 1);
    LcdWriteCommand(LCD_DISPLAY_OFF_CURSOR_OFF_BLINK_OFF, 1);
    LcdWriteCommand(LCD_CLEAR, 1);
    LcdWriteCommand(LCD_INCREMENT_NO_SHIFT, 1);
    LcdWriteCommand(LCD_DISPLAY_ON_CURSOR_OFF, 1);
    //LcdWriteCommand(LCD_OFF, 0);
}

int LCD_CLEAR_SCREEN(void)
{
	LcdWriteCommand(LCD_CLEAR, 1);
	return 0;
}

void LcdWriteCommand(unsigned char data, unsigned char cmdtype)
{
    unsigned char byte;

    byte = (HI_NIBBLE(data) | LCD_BL) | LCD_EN;
    I2cTransmit(0X3F, byte);
   // while(I2cNotReady());

    byte = (HI_NIBBLE(data) | LCD_BL) & ~LCD_EN;
    I2cTransmit(0X3F, byte);
   // while(I2cNotReady());

    // cmdtype = 0 -> One write cycle
    // cmdtype = 1 -> Two write cycles (4 bit mode)

    if (cmdtype)
    {
        byte = (LO_NIBBLE(data) | LCD_BL) | LCD_EN;
        I2cTransmit(0X3F, byte);
      //  while(I2cNotReady());
        byte = (LO_NIBBLE(data) | LCD_BL) | ~LCD_EN;
        if (BACKLIGHT_STATE == LCD_BACKLIGHT_ON)
        	I2cTransmit(0X3F, byte);
        else if (BACKLIGHT_STATE == LCD_BACKLIGHT_OFF)
        	I2cTransmit(0x3F, byte & ~0x08);
      //  while(I2cNotReady());
    }

    __delay_ms(30);
}
void LcdWriteChar(unsigned char data)
{
    unsigned char byte;

    byte = (HI_NIBBLE(data) | LCD_BL | LCD_RS) | LCD_EN;
    I2cTransmit(0X3F, byte);
  //  while(I2cNotReady());

    byte = (HI_NIBBLE(data) | LCD_BL | LCD_RS) & ~LCD_EN;
    I2cTransmit(0X3F, byte);
   // while(I2cNotReady());

    byte = (LO_NIBBLE(data) | LCD_BL | LCD_RS) | LCD_EN;
    I2cTransmit(0X3F, byte);
//    while(I2cNotReady());

    byte = (LO_NIBBLE(data) | LCD_BL | LCD_RS) & ~LCD_EN;
    I2cTransmit(0X3F, byte);
//    while(I2cNotReady());

}
int LCD_PRINT(char *s)
{
    while (*s != '\0')
        LcdWriteChar(*s++);
    return 0;
}
int LCD_SETCURSOR(unsigned char row, unsigned char column)
{
    switch(row)
    {
        case 1:
            LcdWriteCommand(LCD_LINE1 + (column - 1), 1);
            break;
        case 2:
            LcdWriteCommand(LCD_LINE2 + (column - 1), 1);
            break;
        case 3:
            LcdWriteCommand(LCD_LINE3 + (column - 1), 1);
            break;
        case 4:
            LcdWriteCommand(LCD_LINE4 + (column - 1), 1);
            break;
        default:
            LcdWriteCommand(LCD_LINE1 + (column - 1), 1);
    }
    return 0;
}


/* ----------------------------------------------------------------------------
 * Function      : void Button_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : Interrupt handler triggered by a button press. Cancel any
 *                 ongoing transfers, switch to Master mode and start a
 *                 MasterTransmit operation through I2C.
 * Inputs        : event - event number which triggered the callback
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Button_EventCallback(uint32_t event)
{
    static bool ignore_next_dio_int = false;
    if (ignore_next_dio_int)
    {
        ignore_next_dio_int = false;
    }
    /* Button is pressed: Ignore next interrupt.
     * This is required to deal with the debounce circuit limitations. */
    else if (event == GPIO_EVENT_0_IRQ)
    {
		ignore_next_dio_int = true;
        /* Abort current transfer */
        i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);

        /* Start transmission as master */
        i2c->MasterTransmit(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_tx, buff_size, false);
        PRINTF("BUTTON PRESSED: START TRANSMISSION\n");
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_MasterCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a master callback. The parameter event
 *                 indicates one or more events that occurred during driver
 *                 operation.
 * Inputs        : event - I2C Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_MasterCallback(uint32_t event)
{
    bool direction = (i2c->GetStatus().direction == 1U);

    /* Check if transfer is done */
    if (event & ARM_I2C_EVENT_TRANSFER_DONE)
    {
        /* Device is configured as transmitter */
        if (direction == I2C_STATUS_DIRECTION_TX)
        {
            /* Delay time for switching modes of operation */
            //Sys_Delay_ProgramROM((uint32_t)(0.5/1000 * SystemCoreClock));

            /* MasterTransmit finished, start MasterReceive */
            i2c->MasterReceive(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_rx, buff_size, false);
        }
        /* Check if device is configured as receiver */
        else if (direction == I2C_STATUS_DIRECTION_RX)
        {
            /* Check the address nack event */
            if (event & ARM_I2C_EVENT_ADDRESS_NACK)
            {
                /* MasterTransmit finished, start MasterReceive */
                i2c->MasterReceive(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_rx, buff_size, false);
                return;
            }

            /* Delay time for switching modes of operation */
            //Sys_Delay_ProgramROM((uint32_t)(0.5/1000 * SystemCoreClock));

            /* MasterReceive finished, go back to SlaveReceive default mode */
            /* Enter slave receiver mode */
            i2c->SlaveReceive(buff_rx, buff_size);

            /* Blink LED if received data matches I2C_TX_DATA */
            if (!strcmp((const char *)buff_rx, (const char *)buff_tx))
            {
                /* Toggle LED state 2 times for 500 milliseconds */
                ToggleLed(2, 500);
                PRINTF("LED BLINKED: CORRECT_DATA_RECEIVED\n");
            }
        }
    }
    /* Check if transfer error occurred */
//    else if ((event & ARM_I2C_EVENT_TRANSFER_INCOMPLETE) ||
//             (event & ARM_I2C_EVENT_BUS_ERROR))
//    {
//        /* Abort current transfer */
//        i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);
//
//        /* Go back to SlaveReceive default mode */
//        i2c->SlaveReceive(buff_rx, buff_size);
//
//        /* Toggle LED state 10 times for 100 milliseconds to indicate error */
//        ToggleLed(10, 100);
//        PRINTF("TRANSFER INCOMPLETE OR BUS ERROR\n");
//    }
}


/* ----------------------------------------------------------------------------
 * Function      : void I2C_CallBack(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. The parameter event indicates one or more events
 *                 that occurred during driver operation.
 * Inputs        : event - I2C Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_EventCallback(uint32_t event)
{
    static volatile uint32_t I2C_Event;
    bool mode = (i2c->GetStatus().mode == 1U);
    I2C_Event |= event;

    /* Refresh the watchdog */
    Sys_Watchdog_Refresh();

    if(mode == I2C_STATUS_MODE_MASTER)
    {
        I2C_MasterCallback(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ToggleLed(uint32_t n, uint32_t delay_ms)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the led pin.
 * Inputs        : n        - number of toggles
 *               : delay_ms - delay between each toggle [ms]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ToggleLed(uint32_t n, uint32_t delay_ms)
{
    for (; n > 0; n--)
    {
        /* Refresh the watchdog */
        Sys_Watchdog_Refresh();

        /* Toggle led diode */
        gpio->ToggleValue(LED_DIO);

        /* Delay */
        Sys_Delay_ProgramROM((delay_ms / 1000.0) * SystemCoreClock);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, switching to
 *                 the 48 MHz clock and configuring the required DIOs to use
 *                 the i2c.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to 48 MHz oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Initialize gpio structure */
    gpio = &Driver_GPIO;

    /* Initialize gpio driver */
    gpio->Initialize(Button_EventCallback);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Initialize i2c driver structure */
    i2c = &Driver_I2C0;

    /* Initialize i2c, register callback function */
    i2c->Initialize(I2C_EventCallback);

    LcdInit();

}

