#include "app.h"

#define ON           0x01
#define OFF          0x00

extern int LCD_INIT();
extern int LCD_BACKLIGHT(unsigned char value);
extern int LCD_SETCURSOR(unsigned char row, unsigned char colum);
extern int LCD_PRINT(char *Data);
extern int LCD_CLEAR_SCREEN();
extern void Initialize(void);
extern void __delay_ms(float delay);
extern unsigned char intToStr(int value);
extern unsigned char floatToStr(float value);
