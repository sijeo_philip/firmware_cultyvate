I2C CMSIS-Driver Sample Code
============================

NOTE: If you use this sample application for your own purposes, follow the
      licensing agreement specified in `Software_Use_Agreement.rtf` in the
      home directory of the installed Software Development Kit (SDK).

Overview
--------

This sample project demonstrates how to use the I2C CMSIS-Driver to transfer
data in both master and slave modes between two RSL10 Evaluation and 
Development Boards. 

To use this sample application, the `ARM.CMSIS` pack must be installed in your 
IDE. In **CMSIS Pack Manager**, on the right panel, you can see the **Packs 
and Examples** view. In the **Packs** view, you will see **CMSIS packs**. Find 
`ARM.CMSIS` and click on the **Install** button.

Additional information about the application:

1)  Upon starting, the application configures the driver to wait for the 
    reception of data in slave mode by calling the `SlaveReceive` function.

2) 	When the DIO5 button on one of the Evaluation boards is pressed, the
    application aborts the `SlaveReceive` operation and starts transmitting as
    the master by calling the `MasterTransmit` function.

3)  The application transfers the string message "RSL10 I2C TEST".

4)  Upon a successful transfer, the I2C CMSIS-Driver sends an 
    `ARM_I2C_EVENT_TRANSFER_DONE` to the application, using a callback 
    function. The event is generated on both master and slave sides.

5)  When receiving `ARM_I2C_EVENT_TRANSFER_DONE`, the application does the 
    following:

* On the receiving side, it blinks the LED (DIO6) for 0.5 seconds, to indicate
  the received message matches the expected string: "RSL10 includes an I2C".
* On the master side, if it has finished performing `MasterTransmit`, it now
  demonstrates a `MasterReceive` operation.
* On the slave side, if it has finished performing `SlaveReceive`, it now
  demonstrates a `SlaveTransmit` operation.

6)  If a bus error is detected, the application aborts the transfer and blinks 
    the LED 5 times very fast (for 0.05 seconds).

7)  RSL10 is configured to execute at 48 MHz, with a bus speed of 100 kHz. The
    bus speed can be configured in `app.h`.
    
Hardware Requirements
---------------------
Two RSL10 Evaluation and Development Boards are required. Connect them as
follows:

        Board #1             Board #2
    I2C0_SCL  (SCL)   ->    I2C0_SCL (SCL)
    PI2C0_SDA (SDA)   ->    I2C0_SDA (SDA)

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.
  
Verification
------------
To verify that the application is working correctly, load the sample code on
both boards and connect them as detailed in the *Hardware Requirements* 
section. Press the push button on one of the boards (DIO5). The LED (DIO6) 
blinks sequentially, one time on each board, confirming that both boards have 
transmitted and received the string message correctly.
 
The source code exists in `app.c` and `app.h`. The driver configuration is 
specified in `RTE_Device.h`.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1)  Connect DIO12 to ground.

2)  Press the RESET button (this restarts the application, which pauses at the 
    start of its initialization routine).

3)  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can now work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
